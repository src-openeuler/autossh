Name: autossh
Version: 1.4g
Release: 1
License: BSD
Summary: Utility to autorestart SSH tunnels
URL: https://www.harding.motd.ca/autossh/
Source0: https://www.harding.motd.ca/autossh/autossh-1.4g.tgz
Source1: autossh.service
BuildRequires:  gcc
BuildRequires:  openssh-clients
BuildRequires:  systemd
Requires: shadow-utils openssh-clients

%description
autossh is a utility to start and monitor an ssh tunnel. If the tunnel
dies or stops passing traffic, autossh will automatically restart it.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/autossh
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p examples

cp -p autossh.host rscreen examples
chmod 0644 examples/*

install -m 0755 -p autossh $RPM_BUILD_ROOT%{_bindir}
cp -p autossh.1 $RPM_BUILD_ROOT%{_mandir}/man1

install -m 0644 -p %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}

%pre
getent group autossh >/dev/null || groupadd -r autossh
getent passwd autossh >/dev/null || \
    useradd -r -g autossh -d %{_sysconfdir}/autossh -s %{_sbindir}/nologin \
    -c "autossh service account" autossh
exit 0

%post
%systemd_post autossh.service

%preun
%systemd_preun autossh.service

%postun
%systemd_postun_with_restart autossh.service

%files
%doc CHANGES README 
%doc examples
%{_bindir}/*
%attr(750,autossh,autossh) %dir %{_sysconfdir}/autossh/
%{_mandir}/man1/*
%{_unitdir}/*

%changelog
* Mon Nov 18 2024 sqfu <dev01203@linx-info.com> - 1.4g-1
- init package
